import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import { createChanel ,showNotification, handleCancel, handleScheduleNotification } from './src/notification.android';
const App = () => {
  createChanel();
  return (
    <View style={styles.container}>
      <Text>Push notification</Text>
      <TouchableOpacity activeOpacity={0.6} onPress={() => {
        showNotification('hello', 'message');
      }}>
        <View style={styles.button}>
          <Text style={styles.buttonTitle}>Click me to get notification</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.6} onPress={() => {
        handleScheduleNotification('hi', 'showed after 5 secs')
      }}>
        <View style={styles.button}>
          <Text style={styles.buttonTitle}>Click me to get notification after 5 sec</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.6} onPress={handleCancel}>
        <View style={styles.button}>
          <Text style={styles.buttonTitle}>Click me to cancel notification</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    padding: 16,
    backgroundColor: 'blue',
    borderRadius: 24,
    marginTop: 10,
  },
  buttonTitle: {
    color: 'white'
  }
}
);