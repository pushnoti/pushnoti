import PushNotification from 'react-native-push-notification'

const showNotification = (title, message) => {
    PushNotification.localNotification({
        channelId: "channel-id",
        title: title,
        message: message,
    })
    console.log('a');
}
const createChanel = () => {
    PushNotification.createChannel(
        {
          channelId: "channel-id", // (required)
          channelName: "My channel", // (required)
          channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
          soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
          importance: 4, // (optional) default: 4. Int value of the Android notification importance
          vibrate: true, // (optional) default: true. Creates the default vibration patten if true.
        },
        (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
      );
}
const handleScheduleNotification = (title, message) => {
    PushNotification.localNotification({
        channelId: "channel-id",
        title: title,
        message: message,
        date: new Date(Date.now() + 5 * 1000),
    })
}

const handleCancel = () => {
    PushNotification.cancelAllLocalNotifications();
}
export {showNotification ,handleCancel ,handleScheduleNotification, createChanel};